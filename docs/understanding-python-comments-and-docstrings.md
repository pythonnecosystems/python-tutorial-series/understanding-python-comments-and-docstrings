# Python 주석과 Docstrings의 이해 <sup>[1](#footnote_1)</sup>

## 소개
이 글에서는 다음 내용을 공부한다:

- 주석이란 무엇이며 Python 프로그래밍에서 주석이 중요한 이유
- Python에서 한 줄 또는 여러 줄 주석을 작성하는 방법
- docstring이란 무엇이며 주석과 어떻게 다른가?
- Python에서 모듈, 함수, 클래스 및 메서드에 대한 docstring을 작성하는 방법
- Python에서 docstring을 액세스하고 사용하는 방법
- Sphinx와 Pydoc과 같은 도구를 사용하여 docstring에서 문서를 생성하는 방법

이 튜토리얼이 끝나면 Python 코드에 대한 명확하고 간결한 주석과 docstring을 작성하고 이를 사용하여 프로젝트에 대한 전문적인 문서를 만들 수 있다.

이제 시작하자!

## 주석이란 무엇이며, 중요한 이유?
주석은 코드에서 Python 인터프리터가 무시하는 텍스트 줄이다. 주석은 코드에 메모, 설명 또는 지침을 추가하는 데 사용되어 코드를 더 쉽게 이해하고 유지 관리할 수 있게 해준다. 주석은 코드의 일부를 삭제하지 않고 일시적으로 비활성화하거나 디버깅하는 데에도 사용할 수 있다.

주석은 다음과 같은 이유로 중요하다.

- 특히 코드가 복잡하거나 많은 단계를 포함하는 경우 또는 다른 프로그래머가 코드를 읽고 이해하는 데 도움이 된다.
- 코드의 목적, 기능과 논리를 문서화하여 재사용 및 수정이 더 수워해진다.
- 코드의 가독성과 스타일을 개선하여 보다 일관성 있고 전문적인 코드를 만들 수 있다.
- 다양한 시나리오나 입력을 테스트하여 코드의 오류나 버그를 찾아 수정하는 데 도움이 될 수 있다.

따라서 좋은 코멘트를 작성하는 것은 장기적으로 시간과 노력을 절약할 수 있으므로 모든 Python 프로그래머에게 필수적인 기술이다.

## Python에서 한 줄 또는 여러 줄 주석 작성
Python에서는 한 줄 주석과 여러 줄 주석의 두 가지 방법으로 주석을 작성할 수 있다.

한 줄 주석은 해시 기호(`#`)로 시작하고 새줄(return) 문자(`\n`)로 끝나는 텍스트 한 줄이다. Python 인터프리터는 해시 기호 뒤부터 줄 끝까지의 모든 내용을 무시한다. 한 줄 주석을 사용하여 코드에 짧은 메모나 설명을 추가하거나 코드 줄을 일시적으로 비활성화시킬 수 있다.

예를 들어 다음 코드는 한 줄 주석을 사용하여 각 줄의 기능을 설명하고 있다.

```python
# This is a single-line comment
print("Hello, World!") # This prints a message to the screen
# print("This line is commented out and will not run")
```

여러 줄 주석은 여러 줄에 걸쳐 있고 작은따옴표(`'''`) 3개 또는 큰따옴표(`"""`) 3개로 묶인 텍스트 블록이다. Python 인터프리터는 여는 따옴표와 닫는 따옴표 사이의 모든 내용을 무시한다. 여러 줄 주석을 사용하여 코드에 긴 설명이나 지침을 추가하거나 코드 블록을 일시적으로 비활성화할 수 있다.

예를 들어 다음 코드는 여러 줄 주석을 사용하여 함수의 목적을 설명하고 있다.

```python
"""
This is a multi-line comment
This function takes a name as an argument and returns a greeting message
"""
def greet(name):
    return "Hello, " + name + "!"
```

보다시피 Python에서 코멘트를 작성하는 것은 간단하고 직관적이다. 하지만 다음과 같은 몇몇 모범 사례를 따라 주석을 작성해야 한다.

- 코드의 내용, 이유, 방법을 설명하는 명확하고 간결한 주석을 작성하자.
- 뻔하거나 중복되지 않고 독자에게 관련성이 있고 도움이 되는 코멘트를 작성하자.
- 일관성 있고 표준 형식이나 스타일을 따르는 댓글을 작성한다.
- 코드와 함께 업데이트 및 유지 관리되는 주석을 작성하자.
- 다음 섹션에서는 Python에서 주석을 작성하는 또 다른 방법인 docstring에 대해 알아보겠다.

## docstring란 무엇이며, 주석과 어떻게 다른가?
docstring은 Python에서 모듈, 함수, 클래스 및 메서드의 기능과 사용법을 문서화하는 데 사용되는 특수한 유형의 주석이다. 문서화되는 객체의 정의의 시작과 끝은 3개의 작은따옴표(`'''`) 또는 3개의 큰따옴표(`"""`)로 묶여 있다. 문서 문자열은 Python 인터프리터뿐만 아니라 코드에서 문서를 생성하는 외부 도구에서도 액세스하고 사용할 수 있다.

docstring은 여러 가지 면에서 주석과 다르다.

- docstring은 목적, 매개변수, 반환값, 예외 등 문서화하는 객체의 사양을 설명하기 위한 것이다. 주석은 로직, 단계 또는 알고리즘과 같은 코드의 구현을 설명하기 위한 것이다.
- docstring은 문서화하는 객체의 정의 내부, 헤더 줄 바로 뒤에 배치된다. 주석은 참조하는 객체의 앞이나 뒤에 코드의 어느 곳에나 배치할 수 있다.
- Python 인터프리터는 내장 함수 `help()`와 `__doc__`를 사용하여 docstring를 액세스하고 사용할 수 있다. 주석은 Python 인터프리터에 의해 무시되며 액세스하거나 사용할 수 없다.
- docstring은 Sphinx나 Pydoc과 같은 외부 도구에서 코드의 문서를 생성하는 데 사용할 수 있다. 이러한 도구에서는 주석을 사용할 수 없으며 문서에 포함되지 않는다.

따라서 docstring은 Python 인터프리터와 외부 도구 모두에서 액세스하고 사용할 수 있으므로 파이썬 코드에 대한 문서를 작성하는 강력하고 편리한 방법이다. 다음 섹션에서는 Python에서 다양한 타입의 객체에 대한 docstring을 작성하는 방법을 알아본다.

## Python에서 모듈, 함수, 클래스, 메서드에 대한 docstring 작성
이 섹션에서는 모듈, 함수, 클래스, 메서드 등 Python에서 다양한 타입의 객체에 대한 docstring을 작성하는 방법을 설명한다. 또한 PEP 257과 Google 스타일 가이드같은 일반적인 docstring 작성 규칙과 형식에 대해서도 알아본다.

모듈은 정의, 문, 임포트와 같은 Python 코드가 포함된 파일이다. 모듈 docstring은 모듈 파일의 맨 위에 다른 코드보다 먼저 작성하는 docstring이다. 모듈 docstring는 모듈에 대한 간략한 개요, 목적과 내용을 제공할 수 있어야 한다. 또한 작성자, 라이선스, 종속성 및 사용 예와 같은 정보도 포함할 수 있다.

예를 들어 다음 코드는 사람에게 인사하는 함수를 정의하는 `greet.py`라는 모듈의 모듈 docstring를 보여 준다.

```python
"""
This is a module docstring for greet.py
This module defines a function to greet a person with a message.
Author: John Doe
License: MIT
Dependencies: None
Usage: import greet
        greet.greet("World")
"""

def greet(name):
    """This is a function docstring for greet."""
    print("Hello, " + name + "!")
```

함수는 특정 작업을 수행하는 코드 블록으로 재사용할 수 있다. 함수 docstring은 함수 헤더 줄 바로 뒤, 함수 본문 앞에 나타나는 docstring이다. 함수 docstring은 매개변수, 반환값, 예외, 예 등 함수의 기능과 사용법을 설명해야 한다.

예를 들어 다음 코드는 이전 모듈에서 정의한 `greet` 함수에 대한 함수 docstring을 보여준다.

```python
def greet(name):
    """
    This is a function docstring for greet.
    This function takes a name as an argument and prints a greeting message to the screen.
    Parameters:
        name (str): The name of the person to greet.
    Returns:
        None
    Raises:
        TypeError: If name is not a string.
    Examples:
        >>> greet("World")
        Hello, World!
        >>> greet(42)
        TypeError: name must be a string
    """
    if not isinstance(name, str):
        raise TypeError("name must be a string")
    print("Hello, " + name + "!")
```

클래스는 어트리뷰트와 메서드가 있는 객체를 만들기 위한 청사진이다. 클래스 docstring은 클래스 헤더 줄 바로 뒤, 클래스 본문 앞에 나타나는 docstring이다. 클래스 docstring에는 속성, 메서드, 상속, 예제 등 클래스의 목적과 기능을 설명해야 한다.

예를 들어 다음 코드는 내장 클래스 `object`로부터 상속되고 `name` 속성과 `greet` 메서드가 있는 `Person`이라는 클래스에 대한 클래스 docstring을 보여준다.

```python
class Person(object):
    """
    This is a class docstring for Person.
    This class represents a person with a name and a greet method.
    Attributes:
        name (str): The name of the person.
    Methods:
        greet(): Prints a greeting message to the screen.
    Inheritance:
        This class inherits from the built-in class object.
    Examples:
        >>> p = Person("John")
        >>> p.name
        'John'
        >>> p.greet()
        Hello, John!
    """
    def __init__(self, name):
        """This is a method docstring for __init__."""
        self.name = name

    def greet(self):
        """This is a method docstring for greet."""
        print("Hello, " + self.name + "!")
```

메서드는 클래스 내부에 정의되고 객체에 속하는 함수이다. 메소드 docstring은 메소드 헤더 줄 바로 뒤, 메소드 본문 앞에 나타나는 docstring이다. 메소드 docstring은 함수 docstring과 유사하게 메소드의 기능과 사용법을 설명해야 한다. 그러나 메서드 docstring에는 객체 자체를 가리키는 자체 매개변수와 객체의 클래스를 가리키는 클래스 매개변수에 대한 정보도 포함될 수 있다.

예를 들어 다음 코드는 이전 클래스에서 정의된 `__init__`와 `greet` 메서드에 대한 메서드 docstring을 보여준다.

```python
class Person(object):
    # Class docstring omitted for brevity

    def __init__(self, name):
        """
        This is a method docstring for __init__.
        This method initializes a new Person object with a name attribute.
        Parameters:
            self (Person): The object itself.
            name (str): The name of the person.
        Returns:
            None
        """
        self.name = name

    def greet(self):
        """
        This is a method docstring for greet.
        This method prints a greeting message to the screen using the name attribute.
        Parameters:
            self (Person): The object itself.
        Returns:
            None
        """
        print("Hello, " + self.name + "!")
```

보다시피, 몇 가지 기본 규칙과 관행을 따르기만 한다면 Python에서 다양한 타입의 객체에 대한 docstring을 작성하는 것은 그리 어렵지 않다. 그러나 PEP 257과 Google 스타일 가이드와 같이 docstring의 내용과 구조에 대한 기본 설정과 권장 사항이 다른 다양한 스타일과 형식이 있다. 일관성 있고 명확하기만 하다면 자신의 필요와 선호도에 맞는 스타일과 형식을 선택할 수 있다.

다음 섹션에서는 기본 제공 함수인 `help()`와 ` __doc__`을 사용하여 Python에서 docstring을 액세스하고 사용하는 방법을 설명한다.

## Python에서 docstring 액세스와 사용법
이 섹션에서는 내장된 함수 `help()`와 `__doc__`을 사용하여 Python에서 docstring에 액세스하고 사용하는 방법을 살펴볼 것이다.

`help()` 함수는 Python에서 모듈, 함수, 클래스, 메소드, 변수 등 어떤 객체의 문서도 디스플레이하는 유용한 도구이다. `help()` 함수는 객체를 인수로 전달하거나 대화형 도움말 모드로 진입하는 두 가지 방식으로 사용할 수 있다.

예를 들어, 이전 섹션에서 정의한 `greet` 함수를 `help()` 함수에 인수로 전달하면 그 함수의 설명서를 볼 수 있다.

```python
>>> from greet import Person # Import the greet module
>>> help(Person.greet) # Pass the greet function as an argument to the help function
Help on function greet in module greet:

greet(name)
    This is a function docstring for greet.
    This function takes a name as an argument and prints a greeting message to the screen.
    Parameters:
        name (str): The name of the person to greet.
    Returns:
        None
    Raises:
        TypeError: If name is not a string.
    Examples:
        >>> greet("World")
        Hello, World!
        >>> greet(42)
        TypeError: name must be a string
```

보다시피 `help()` 함수는 `greet` 함수의 `docstring`을 모듈 이름, 함수 이름와 함수 시그니처같은 추가 정보와 함께 디스플레이한다.

대화형 도움말 모드로 진입하고자 하는 경우, 아무런 인수 없이 `help()` 함수를 호출하면 된다. 이렇게 하면 임의의 객체 이름을 입력하고 해당 객체의 문서를 볼 수 있는 프롬프트가 열릴 것이다. `modules`, `keywords`, `topics`, `quit` 등의 키워드를 사용하여 help 시스템을 탐색할 수도 있다.

예를 들어, 이전 섹션에서 정의한 `Person` 클래스의 문서를 보려면 대화형 도움말 모드로 들어가 클래스 이름을 입력하여야 한다.

```python
>>> help() # Call the help function without any argument to enter the interactive help mode

Welcome to Python 3.9's help utility!

If this is your first time using Python, you should definitely check out
the tutorial on the Internet at https://docs.python.org/3.9/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules. To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, symbols, or topics, type
"modules", "keywords", "symbols", or "topics". Each module also comes
with a one-line summary of what it does; to list the modules whose name
or summary contain a given string such as "spam", type "modules spam".

help> person.Person # Type the name of the class to see its documentation
Help on class Person in module __main__:

class Person(builtins.object)
    |  This is a class docstring for Person.
    |  This class represents a person with a name and a greet method.
    |  Attributes:
    |      name (str): The name of the person.
    |  Methods:
    |      greet(): Prints a greeting message to the screen.
    |  Inheritance:
    |      This class inherits from the built-in class object.
    |  Examples:
    |      >>> p = Person("John")
    |      >>> p.name
    |      'John'
    |      >>> p.greet()
    |      Hello, John!
    |  
    |  Methods defined here:
    |  
    |  __init__(self, name)
    |      This is a method docstring for __init__.
    |      This method initializes a new Person object with a name attribute.
    |      Parameters:
    |          self (Person): The object itself.
    |          name (str): The name of the person.
    |      Returns:
    |          None
    |  
    |  greet(self)
    |      This is a method docstring for greet.
    |      This method prints a greeting message to the screen using the name attribute.
    |      Parameters:
    |          self (Person): The object itself.
    |      Returns:
    |          None
    |  
    |  ----------------------------------------------------------------------
    |  Data descriptors defined here:
    |  
    |  __dict__
    |      dictionary for instance variables (if defined)
    |  
    |  __weakref__
    |      list of weak references to the object (if defined)

help> quit # Type quit to exit the interactive help mode
You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)".  Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.
>>>
```

보다시피 대화형 도움말 모드는 사용자 클래스의 docstring을 모듈 이름, 클래스 이름, 상속, 메서드 및 속성과 같은 추가 정보와 함께 디스플레이한다.

`__doc__` 속성은 Python에서 임의 객체의 docstring에 접근하는 또 다른 방법이다. `__doc__` 속성은 객체의 docstring을 포함하는 문자열이거나 객체에 docstring이 없으면 `None`이다. `__doc__` 속성을 사용하여 임의 객체의 docstring을 인쇄, 저장 또는 조작할 수 있다.

예를 들어, 이전 섹션에서 정의한 `greet` 함수의 docstring을 인쇄하려면 `__doc__` 속성을 사용할 수 있다.

```python
>>> import greet # Import the greet module
>>> print(greet.greet.__doc__) # Print the docstring of the greet function using the __doc__ attribute
    This is a function docstring for greet.
    This function takes a name as an argument and prints a greeting message to the screen.
    Parameters:
        name (str): The name of the person to greet.
    Returns:
        None
    Raises:
        TypeError: If name is not a string.
    Examples:
        >>> greet("World")
        Hello, World!
        >>> greet(42)
        TypeError: name must be a string
```

보다시피 `__doc__` 속성은 `greet` 함수의 docstring을 문자열로 반환하며, 이를 인쇄하거나 다른 용도로 사용할 수 있다.

Python에서 docstrings에 접근하고 사용하는 것은 매우 쉽고 편리하며, 내장된 함수 `help()`와 `__doc__`를 사용하여 Python에서 어떤 객체의 문서를 디스플레이, 접근 또는 조작할 수 있기 때문이다. 다음 절에서는 Sphinx나 Pydoc과 같은 도구를 사용하여 docstrings에서 문서를 생성하는 방법을 설명할 것이다.

## Sphinx 또는 Pydoc과 같은 도구를 사용하여 docstring 문서 생성
이 절에서는 Sphinx나 Pydoc과 같은 도구를 사용하여 docstring에서 문서를 생성하는 방법을 설명할 것이다. 코드에 작성한 docstring을 사용하면 이러한 도구를 사용하여 Python 프로젝트를 위한 전문적이고 포괄적인 문서를 생성할 수 있다.

Sphinx는 HTML, PDF, ePub 및 기타 문서 형식을 docstring과 다른 소스(예: reStructuredText 파일)로부터 생성할 수 있는 도구이다. 또한 Sphinx는 autodic, napoleon 및 intersphinx 같은 다른 도구와 통합하여 코드에서 docstring을 자동으로 추출하고, 다양한 문서 docstring과 형식을 지원하며, 외부 문서에 연결할 수 있다.

Sphinx를 사용하려면 pip 또는 conda를 사용하여 설치한 다음 `spinx-quickstart` 명령을 실행하여 기본 구성 파일과 소스 디렉토리를 생성해야 한다. 그런 다음 구성 파일을 편집하여 프로젝트 이름, 작성자, 테마, 확장자 등 문서에 대한 옵션과 설정을 커스터마이징할 수 있다. 또한 ReStructuredText 파일과 같은 다른 문서 소스를 소스 디렉토리에 추가하고 지침과 역할을 사용하여 docstring과 기타 정보를 포함할 수 있다. 마지막으로 `make` 명령을 실행하여 원하는 형식으로 문서를 빌드할 수 있다.

예를 들어, 이전 섹션에서 정의한 `greet` 모듈과 `Person` 클래스에 대한 HTML 문서를 생성하려면 다음 단계를 수행한다.

1. pip 또는 conda를 사용하여 sphinx를 설치한다.

```bash
$ pip insatll sphinx # or conda install sphinx
```

2. `spinx-quickstart` 명령을 실행하고 프롬프트에 응답하여 기본 구성 파일과 소스 디렉터리를 만든다.

```bash
> sphinx-quickstart
Welcome to the Sphinx 4.1.2 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]: n

The project name will occur in several places in the built documentation.
> Project name: Greet and Person
> Author name(s): John Doe
> Project release []: 1.0

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
> Project language [en]: en

Creating file ./conf.py.
Creating file ./index.rst.
Creating file ./Makefile.
Creating file ./make.bat.

Finished: An initial directory structure has been created.

You should now populate your master file ./index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
    make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
```

3. 구성 파일 `conf.py` 을 편집하여 `spinx.ext.autodoc`, `spinx.ext.text.napoleon` 및 `spinx.ext.interspinx` 같은 필요한 확장자를 사용할 수 있다. 테마, 로고, 사이드바 등과 같은 다른 옵션과 설정을 사용자 정의할 수도 있다. 예를 들어, 파일의 끝에 다음 행을 추가할 수 있다.

```
# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = [
    'sphinx.ext.autodoc', # to automatically extract docstrings from your code
    'sphinx.ext.napoleon', # to support Google and NumPy style docstrings
    'sphinx.ext.intersphinx', # to link to external documentation
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = 'Greet and Person'
author = 'John Doe'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '1.0'
# The full version, including alpha/beta/rc tags.
release = '1.0'

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
# keep_warnings = False

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = False

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster' # you can choose other themes, such as 'sphinx_rtd_theme'

# Theme options are theme-specific and customize the look and feel of a     
# theme. For example, you can add these lines to the end of the file:
html_theme_options = {
    'logo': 'logo.png',
    'github_user': 'username',
    'github_repo': 'reponame',
    'github_banner': True,
    'show_related': False,
    'note_bg': '#FFF59C'
}

# The name of an image file (relative to this directory) to place at the top of
# the sidebar.
html_logo = 'logo.png'

# These folders are copied to the documentation's HTML output
html_static_path = ['_static']

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/custom.css',
]

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
html_last_updated_fmt = '%b %d, %Y'

# If false, no module index is generated.
html_domain_indices = False

# If false, no index is generated.
html_use_index = False

# If true, the index is split into individual pages for each letter.
html_split_index = False

# If true, links to the reST sources are added to the pages.
html_show_sourcelink = False
```

## 마치며
이 포스팅에서는 Python에서 주석과 docstring을 작성하는 방법과 코드를 문서화하고 문서를 생성하는 데 사용하는 방법을 배웠다.

- Python 프로그래밍에서 주석은 무엇이며 왜 중요한가
- Python에서 한 줄과 여러 줄 주석을 작성하는 방법
- docstring이란 무엇이며 주석과 어떻게 다른가
- Python에서 모듈, 함수, 클래스와 메서드에 대한 docstring을 작성하는 방법
- 내장된 함수 `help()`와 `__doc__`를 사용하여 Python에서 docstring에 액세스하고 사용하는 방법
- Spinks와 Pydoc과 같은 도구를 사용하여 docstring으로부터 문서를 생성하는 방법

이를 수행하면 Python 코드에 대한 명확하고 간결한 주석과 docstring을 작성하고 이를 사용하여 프로젝트에 대한 전문적이고 포괄적인 문서를 만들 수 있다.
