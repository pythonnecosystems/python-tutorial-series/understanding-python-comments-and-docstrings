# Python 주석과 Docstrings의 이해 <sup>[1](#footnote_1)</sup>

> <font size="3">변수를 선언하고 다양한 데이터 타입을 사용하는 방법에 대해 알아본다.</font>

## 목차
1. [소개](./understanding-python-comments-and-docstrings.md#소개)
1. [주석이란 무엇이며, 중요한 이유?](./understanding-python-comments-and-docstrings.md#댓글이란-무엇이며-중요한-이유)
1. [Python에서 한 줄 또는 여러 줄 주석 작성](./understanding-python-comments-and-docstrings.md#python에서-한-줄-또는-여러-줄-주석-작성)
1. [DocStrings란 무엇이며, 주석과 어떻게 다른가?](./understanding-python-comments-and-docstrings.md#docstrings란-무엇이며-주석과-어떻게-다른가)
1. [Python에서 모듈, 함수, 클래스, 메서드에 대한 Docstrings 작성](./understanding-python-comments-and-docstrings.md#python에서-모듈-함수-클래스-메서드에-대한-docstrings-작성)
1. [Python에서 Docstrings 액세스와 사용법](./understanding-python-comments-and-docstrings.md#python에서-docstrings-액세스와-사용법)
1. [Sphinx 또는 Pydoc과 같은 도구를 사용하여 Docstrings 문서 생성](./understanding-python-comments-and-docstrings.md#sphinx-또는-pydoc과-같은-도구를-사용하여-docstrings-문서-생성)
1. [마치며](././understanding-python-comments-and-docstrings.md#마치며)


<a name="footnote_1">1</a>: [Python Tutorial 4 — Understanding Python Comments and Docstrings](https://levelup.gitconnected.com/python-tutorial-4-understanding-python-comments-and-docstrings-43939999bfe7)를 편역한 것입니다.
